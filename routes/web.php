<?php

use Illuminate\Support\Facades\Route;



Route::group(['prefix' => 'admin'], function () {
    Route::get('/', 'BackendlayoutController@index')->name('admin.home');

    Route::resource('clientes', 'admin\ClientesController');

    Route::resource('produtos', 'admin\ProdutosController');

    Route::resource('assinaturas', 'admin\AssinaturasController');

    Route::resource('perguntas', 'admin\PerguntasController');

    Auth::routes();
});


//SISTEMA ROUTES

Route::group(['prefix' => 'sistema'], function(){
    Route::get('/', 'sistema\SistemaController@index')->name('sistema.home');

});


//SISTEMA ROUTES


//FRONTEND ROUTES

Route::get('/', 'FrontendController@index')->name('frontend.home');
Route::get('/conheca', 'FrontendController@conheca')->name('frontend.conheca');
Route::get('/como-funciona', 'FrontendController@comofunciona')->name('frontend.comofunciona');
Route::get('/beneficios', 'FrontendController@beneficios')->name('frontend.beneficios');

Route::get('/adquira', 'FrontendController@adquira')->name('frontend.adquira');
Route::get('/suporte', 'FrontendController@suporte')->name('frontend.suporte');


//LOGIN SCREEn
Route::get('/acesse', 'FrontendController@acesse')->name('frontend.acesse');
