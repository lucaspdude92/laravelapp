<footer class="bg-custom-blue">
    <div class="container">

        <nav class="navbar navbar-expand-lg ">
                <div class="col-md-3">
                    <a class="navbar-brand" href="/">
                        <img src="{{asset('images/logowhite.png')}}" class="img-fluid">
                    </a>
                </div>
                <div class="col-md-9">
                    <div class=" float-right" id="">
                      <div class="navbar-nav navbar-color">
                        <a class="nav-item nav-link" href="{{route('frontend.home')}}">HOME <span class="sr-only">(current)</span></a>
                        <a class="nav-item nav-link" href="{{route('frontend.conheca')}}">CONHEÇA</a>
                        <a class="nav-item nav-link" href="{{ route('frontend.comofunciona')}}">COMO FUNCIONA</a>
                        <a class="nav-item nav-link" href="{{route('frontend.beneficios')}}">BENEFÍCIOS</a>
                        <a class="nav-item nav-link" href="{{route('frontend.suporte')}}">SUPORTE</a>
                      </div>
                    </div>
                </div>
          </nav>
          <div class="col-md-12">
              <div class="float-right"><a href="mailto:suporte@meusregistrosmedicos.com.br" class="">suporte@meusregistrosmedicos.com.br</a></div>
              <div class="clearfix"></div>
          </div>
        <!-- NAVBAR  -->
    </div>
</footer>
