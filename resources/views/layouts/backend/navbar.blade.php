<div class="col-md-2">
    <div class="sidebar-container">
        <div class="sidebar">

            <img src="{{asset('images/logowhite.png') }}" class="img-fluid">
            <div class="spacer"></div>
            <ul class="sistema-navbar">

                @guest()
                <li class="sistema-navbar-item"><a href="{{route('login')}}" class="sistema-navbar-item-link">Acessar</a></li>
                <li class="sistema-navbar-item"><a href="{{route('register')}}" class="sistema-navbar-item-link">Cadastrar</a></li>





                @else
            <li class="sistema-navbar-item"><a href="{{route('home')}}" class="sistema-navbar-item-link">Inicio</a></li>


            <li class="sistema-navbar-item">

                <a class="sistema-navbar-item-link" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Sair') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>

            </li>



            <li class="sistema-navbar-item"><a href="{{route('clientes.index')}}" class="sistema-navbar-item-link">Clientes</a></li>
            <li class="sistema-navbar-item"><a href="{{ route('produtos.index') }}" class="sistema-navbar-item-link">Produtos</a></li>
            <li class="sistema-navbar-item"><a href="{{ route('assinaturas.index') }}" class="sistema-navbar-item-link">Assinaturas</a></li>
            <li class="sistema-navbar-item"><a href="{{ route('perguntas.index') }}" class="sistema-navbar-item-link">Perguntas Frequentes</a></li>

                @endguest()
            </ul>
        </div>
    </div>
</div>
