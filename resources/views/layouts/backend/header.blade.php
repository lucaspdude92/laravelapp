<header class='header-container bg-blue'>

    <div class="">
        <nav class="navbar navbar-expand-lg ">
            <div class="col-md-3">
                <a class="navbar-brand" href="/">
                    <img src="{{asset('images/logowhite.png') }}" class="img-fluid">
                </a>
            </div>
            <div class="col-md-9">
                <div class="collapse navbar-collapse  float-right" id="navbarNavAltMarkup">
                  <div class="navbar-nav navbar-color"><a class="nav-item nav-link small" href="#">Página Inicial</a></div>
                  <div class="navbar-nav navbar-color"><a class="nav-item nav-link small" href="#">Meus Dados</a></div>
                  <div class="navbar-nav navbar-color"><a class="nav-item nav-link small" href="#">Suporte</a></div>
                  <div class="navbar-nav navbar-color"><a class="nav-item nav-link small" href="#">Sair</a></div>
                </div>
            </div>
          </nav>

    </div>

</header>
