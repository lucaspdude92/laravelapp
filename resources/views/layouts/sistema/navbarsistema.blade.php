<header class='header-container'>
    <div class="container">
        <!-- NAVBAR -->
        <nav class="navbar navbar-expand-lg">
            <div class="col-md-3">
                <a class="navbar-brand" href="/">
                    <img src="{{asset('images/logo.jpg')}}" class="img-fluid">
                </a>
            </div>
            <div class="col-md-5">
                <div class="collapse navbar-collapse  float-right" id="navbarNavAltMarkup">
                  <div class="navbar-nav navbar-color">
                    <a class="nav-item nav-link" href="{{route('frontend.home')}}">VOLTAR À PÁGINA INICIAL</span></a>
                  </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class=" ">
                    <a class="btn btn-red" href="{{route('frontend.adquira')}}">ADQUIRA</a>
                    <a class="btn btn-blue-autline" href="{{route('frontend.acesse')}}">ACESSE</a>
                </div>
            </div>
          </nav>
        <!-- NAVBAR  -->
    </div>
</header>
