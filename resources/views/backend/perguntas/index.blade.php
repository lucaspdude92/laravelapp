@extends('layouts.backend.app')

@section('content')
@include('backend.utilities.flash')

<h2 class="page-title">Lista de Perguntas Frequentes</h2>

<table class="table table-hover ">
    <thead class="thead-blue">
      <tr>
        <th scope="col">Titulo</th>
        <th scope="col">Conteúdo</th>

        <th scope="col">Ações</th>
      </tr>
    </thead>
    <tbody class="table-custom">
        @foreach ($perguntas as $pergunta )
        <tr>
        <td >{{$pergunta->titulo}}</td>
          <td>{{$pergunta->conteudo}}</td>
          <td>

            <form method="post" action="{{ route('perguntas.destroy', $pergunta->id) }}">
                <a href="{{route('perguntas.show',$pergunta->id)}} " class="btn btn-success">Visualizar</a>
                <a href="{{route('perguntas.edit',$pergunta->id)}}" class="btn btn-info">Editar</a>
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit" role="button">Deletar</button>
              </form>

          </td>
        </tr>
        @endforeach
    </tbody>
  </table>


  <div class="spacer"></div>


  <div class="col-md-10 offset-1">
    <h3>Adicione uma Nova Pergunta</h3>

    <form action="{{route('perguntas.store')}}" method="POST">
        @csrf

            <div class="form-group">
                <label for="titulo">Titulo da Pergunta</label>
                <input type="text" class="form-control" name="titulo" id="titulo" value="{{ old('titulo') }}">
            </div>

        <div class="form-group">
            <label for="conteudo">Resposta à Pergunta</label>
            <textarea class="form-control" name="conteudo" id="conteudo" rows="5"></textarea>
        </div>

        <div class="float-right">
            <button class="btn btn-success" type="submit">Adicionar Nova</button>
        </div>
    </form>
  </div>






@endsection
