@extends('layouts.backend.app')

@section('content')
@include('backend.utilities.flash')

<h2 class="page-title">Pergunta: {{$pergunta->titulo}}</h2>

<table class="table table-hover ">
    <thead class="thead-blue">
      <tr>
        <th scope="col">Titulo</th>
        <th scope="col">Conteúdo</th>
        <th scope="col">Ações</th>
      </tr>
    </thead>
    <tbody class="table-custom">
        <tr>
        <td >{{$pergunta->titulo}}</td>
          <td>{{$pergunta->conteudo}}</td>
          <td>

            <form method="post" action="{{ route('perguntas.destroy', $pergunta->id) }}">
                <a href="{{route('perguntas.edit',$pergunta->id)}}" class="btn btn-info">Editar</a>
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit" role="button">Deletar</button>
              </form>

          </td>
        </tr>
    </tbody>
  </table>






@endsection
