@extends('layouts.backend.app')

@section('content')
@include('backend.utilities.flash')

<h2 class="page-title">Edição da Pergunta: {{$pergunta->titulo}}</h2>



  <div class="col-md-10 offset-1">

    <form action="{{route('perguntas.update', $pergunta->id)}}" method="POST">
        @csrf
        @method('put')

            <div class="form-group">
                <label for="titulo">Titulo da Pergunta</label>
                <input type="text" class="form-control" name="titulo" id="titulo" value="{{ $pergunta->titulo }}">
            </div>

        <div class="form-group">
            <label for="conteudo">Resposta à Pergunta</label>
            <textarea class="form-control" name="conteudo" id="conteudo" rows="5">{{$pergunta->conteudo}}</textarea>
        </div>

        <div class="float-right">
            <button class="btn btn-success" type="submit">Atualizar Pergunta</button>
        </div>
    </form>
  </div>






@endsection
