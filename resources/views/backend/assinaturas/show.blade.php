@extends('layouts.backend.app')

@section('content')
@include('backend.utilities.flash')

<h2 class="page-title">Lista de Assinaturas</h2>

<table class="table table-hover ">
    <thead class="thead-blue">
      <tr>
        <th scope="col">Cliente</th>
        <th scope="col">Protuto</th>
        <th scope="col">Inicio</th>
        <th scope="col">Fim</th>
        <th scope="col">Ações</th>
      </tr>
    </thead>
    <tbody class="table-custom">
        <tr>
            <td >{{$assinatura->clientes->nomecompleto}}</td>
            <td >{{$assinatura->produtos->titulo}}</td>
            <td>{{$assinatura->inicio}}</td>
            <td>{{$assinatura->fim}}</td>
            <td>
                <a href="{{route('assinaturas.edit',$assinatura->id)}}" class="btn btn-info">Editar</a>
            </td>
        </tr>
    </tbody>
  </table>








@endsection
