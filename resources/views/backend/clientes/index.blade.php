@extends('layouts.backend.app')

@section('content')
@include('backend.utilities.flash')


@include('backend.clientes.menubar')

<h2 class="page-title">Lista de Clientes</h2>






<h3></h3>
<table class="table table-hover ">
    <thead class="thead-blue">
      <tr>
        <th scope="col">E-mail</th>
        <th scope="col">Nome</th>
        <th scope="col">Assinatura Válida</th>
        <th scope="col">Ações</th>
      </tr>
    </thead>
    <tbody class="table-custom">
        @foreach ($clientes as $cliente )
        <tr>
        <td >{{$cliente->user->email}}</td>
          <td>{{$cliente->nomecompleto}}</td>
          <td class="small">
            @foreach ($cliente->assinaturas as $assinatura)
                {{$assinatura->produtos->titulo}}
            @endforeach
        </td>
          <td>

            <form method="post" action="{{ route('clientes.destroy', $cliente->id) }}">
                <a href="{{route('clientes.show',$cliente->id)}} " class="btn btn-success">Visualizar</a>
                <a href="{{route('clientes.edit',$cliente->id)}}" class="btn btn-info">Editar</a>
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit" role="button">Deletar</button>
              </form>

          </td>
        </tr>
        @endforeach
    </tbody>
  </table>






@endsection
