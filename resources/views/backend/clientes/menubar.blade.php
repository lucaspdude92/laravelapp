<nav class="navbar navbar-light bg-light">

    <div class="col-md-8">
        <a href="{{route('clientes.index')}}" class="btn btn-success">Todos os clientes</a>
    </div>
    <div class="col-md-4">
        <form class="form-inline">
            <input type="text" placeholder="Busca" class="form-control">
            <button class="btn btn-busca" type="submit">Busca</button>
        </form>
    </div>

</nav>
