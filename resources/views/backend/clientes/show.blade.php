@extends('layouts.backend.app')

@section('content')

@include('backend.utilities.flash')

@include('backend.clientes.menubar')


<h2 class="page-title">Dados do Cliente: <span class="red-accent">{{$cliente->nomecompleto}}</span></h2>


<table class="table ">
      <tr>
        <td scope="col">Nome Completo</td>
        <td scope="col">{{$cliente->nomecompleto}}</td>
      </tr>
      <tr>
        <td scope="col">Data Nascimento</td>
        <td scope="col">{{$cliente->datanascimento}}</td>
      </tr>
      <tr>
        <td scope="col">Naturalidade</td>
        <td scope="col">{{$cliente->naturalidade}}</td>
      </tr>
      <tr>
        <td scope="col">Nacionalidade</td>
        <td scope="col">{{$cliente->nacionalidade}}</td>
      </tr>
      <tr>
        <td scope="col">CEP</td>
        <td scope="col">{{$cliente->cep}}</td>
      </tr>
      <tr>
        <td scope="col">Endereço</td>
        <td scope="col">{{$cliente->endereco}}</td>
      </tr>
      <tr>
        <td scope="col">Número</td>
        <td scope="col">{{$cliente->numero}}</td>
      </tr>
      <tr>
        <td scope="col">Complemento</td>
        <td scope="col">{{$cliente->complemento}}</td>
      </tr>
      <tr>
        <td scope="col">Bairro</td>
        <td scope="col">{{$cliente->bairro}}</td>
      </tr>
      <tr>
        <td scope="col">Cidade</td>
        <td scope="col">{{$cliente->cidade}}</td>
      </tr>
      <tr>
        <td scope="col">Estado</td>
        <td scope="col">{{$cliente->estado}}</td>
      </tr>
      <tr>
        <td scope="col">E-mail</td>
        <td scope="col">{{$cliente->user->email}}</td>
      </tr>
      <tr>
        <td scope="col">Telefone</td>
        <td scope="col">{{$cliente->telefone}}</td>
      </tr>

  </table>


  <div class="spacer"></div>
<nav class="navbar navbar-light bg-light">
    <div class="col-md-12">
        <a href="{{route('clientes.edit',$cliente->id)}}" class="btn btn-info">Editar</a>
        <a href="{{route('clientes.destroy',$cliente->id)}}" class="btn btn-danger">Deletar</a>
    </div>

</nav>







@endsection
