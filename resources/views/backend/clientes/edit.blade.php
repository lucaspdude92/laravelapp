@extends('layouts.backend.app')

@section('content')

@include('backend.utilities.flash')

@include('backend.clientes.menubar')
<h2 class="page-title">Edição dos dados do cliente: <span class="red-accent">{{$cliente->nomecompleto}}</span></h2>






        <form method="post" action="{{route('clientes.update',$cliente->id)}}">

            @method('put')

            @csrf

            <div class="form-group">
                <label for="nomecompleto">Nome Completo</label>
                <input type="text" class="form-control" name="nomecompleto" id="nomecompleto" value="{{$cliente->nomecompleto}}">
            </div>


            <div class="form-group">
                <label for="datanascimento">Data de Nascimento</label>
                <input type="text" class="form-control" name="datanascimento" id="datanascimento" value="{{$cliente->datanascimento}}">
            </div>

            <div class="form-group">
                <label for="naturalidade">Naturalidade</label>
                <input type="text" class="form-control" name="naturalidade" id="naturalidade" value="{{$cliente->naturalidade}}">
            </div>

            <div class="form-group">
                <label for="nacionalidade">Nacionalidade</label>
                <input type="text" class="form-control" name="nacionalidade" id="nacionalidade" value="{{$cliente->nacionalidade}}">
            </div>

            <div class="form-group">
                <label for="cep">CEP</label>
                <input type="text" class="form-control" name="cep" id="cep" value="{{$cliente->cep}}">
            </div>

            <div class="form-group">
                <label for="endereco">Endereço</label>
                <input type="text" class="form-control" name="endereco" id="endereco" value="{{$cliente->endereco}}">
            </div>

            <div class="form-group">
                <label for="numero">Numero</label>
                <input type="text" class="form-control" name="numero" id="numero" value="{{$cliente->numero}}">
            </div>

            <div class="form-group">
                <label for="complemento">Complemento</label>
                <input type="text" class="form-control" name="complemento" id="complemento" value="{{$cliente->complemento}}">
            </div>

            <div class="form-group">
                <label for="bairro">Bairro</label>
                <input type="text" class="form-control" name="bairro" id="bairro" value="{{$cliente->bairro}}">
            </div>

            <div class="form-group">
                <label for="cidade">Cidade</label>
                <input type="text" class="form-control" name="cidade" id="cidade" value="{{$cliente->cidade}}">
            </div>

            <div class="form-group">
                <label for="estado">Estado</label>
                <input type="text" class="form-control" name="estado" id="estado" value="{{$cliente->estado}}">
            </div>

            <div class="form-group">
                <label for="email">Email</label>
                <input type="text" class="form-control" name="email" id="email" value="{{$cliente->email}}">
            </div>

            <div class="form-group">
                <label for="telefone">Telefone</label>
                <input type="text" class="form-control" name="telefone" id="telefone" value="{{$cliente->telefone}}">
            </div>


            <button class="btn btn-success">Alterar</button>

        </form>



@endsection
