@extends('layouts.backend.app')

@section('content')
@include('backend.utilities.flash')

<h2 class="page-title">Lista de Produtos</h2>

<table class="table table-hover ">
    <thead class="thead-blue">
      <tr>
        <th scope="col">Titulo</th>
        <th scope="col">Valor</th>
        <th scope="col">Tipo</th>
        <th scope="col">Ações</th>
      </tr>
    </thead>
    <tbody class="table-custom">
        @foreach ($produtos as $produto )
        <tr>
        <td >{{$produto->titulo}}</td>
          <td>{{$produto->valor}}</td>
          <td>{{$produto->tipo}}</td>
          <td>

            <form method="post" action="{{ route('produtos.destroy', $produto->id) }}">
                <a href="{{route('produtos.show',$produto->id)}} " class="btn btn-success">Visualizar</a>
                <a href="{{route('produtos.edit',$produto->id)}}" class="btn btn-info">Editar</a>
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit" role="button">Deletar</button>
              </form>

          </td>
        </tr>
        @endforeach
    </tbody>
  </table>


  <div class="spacer"></div>


  <div class="col-md-10 offset-1">
    <h3>Adicione um Novo Produto</h3>

    <form action="{{route('produtos.store')}}" method="POST">
        @csrf

        <div class="row">
            <div class="col-md-8">
                <div class="form-group">
                    <label for="titulo">Titulo do Produto</label>
                    <input type="text" class="form-control" name="titulo" id="titulo" value="{{ old('titulo') }}">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="valor">Preço</label>
                    <input type="text" class="form-control" name="valor" id="valor" value="{{ old('valor') }}">
                </div>
            </div>
        </div>
            <div class="form-group">
                <label for="tipo">Tipo de Produto</label>
                <select class="form-control" id="tipo" name="tipo">
                    <option value="Único">Único</option>
                    <option value="Subscrição">Subscrição</option>
                </select>
            </div>



        <div class="form-group">
            <label for="descricao">Descrição do Produto</label>
            <textarea class="form-control" name="descricao" id="descricao" rows="5"></textarea>
        </div>

        <div class="float-right">
            <button class="btn btn-success" type="submit">Adicionar Novo</button>
        </div>
    </form>
  </div>






@endsection
