@extends('layouts.backend.app')

@section('content')
@include('backend.utilities.flash')

<h2 class="page-title">Edição do Produto: {{$produto->titulo}}</h2>



  <div class="col-md-10 offset-1">
    <h3>Adicione um Novo Produto</h3>

    <form action="{{route('produtos.store')}}" method="POST">
        @csrf

        <div class="row">
            <div class="col-md-8">
                <div class="form-group">
                    <label for="titulo">Titulo do Produto</label>
                    <input type="text" class="form-control" name="titulo" id="titulo" value="{{$produto->titulo}}">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="valor">Preço</label>
                    <input type="text" class="form-control" name="valor" id="valor" value="{{ $produto->valor }}">
                </div>
            </div>
        </div>

        <div class="form-group">
            <label for="tipo">Tipo de Produto</label>
            <select class="form-control" id="tipo" name="tipo">
                <option value="Único" @if($produto->tipo) == "Único" selected @endif>Único</option>
                <option value="Subscrição"  @if($produto->tipo) == "Subscrição" selected @endif>Subscrição</option>
            </select>
        </div>



        <div class="form-group">
            <label for="descricao">Descrição do Produto</label>
            <textarea class="form-control" name="descricao" id="descricao" rows="5">{{$produto->descricao}}</textarea>
        </div>

        <div class="float-right">
            <button class="btn btn-success" type="submit">Alterar</button>
        </div>
    </form>
  </div>






@endsection
