@extends('layouts.backend.app')

@section('content')
@include('backend.utilities.flash')

<h2 class="page-title">Produto: {{$produto->titulo}}</h2>

<table class="table table-hover ">
    <thead class="thead-blue">
      <tr>
        <th scope="col">Titulo</th>
        <th scope="col">Valor</th>
        <th scope="col">Descrição</th>
        <th scope="col">Ações</th>
      </tr>
    </thead>
    <tbody class="table-custom">
        <tr>
        <td >{{$produto->titulo}}</td>
          <td>{{$produto->valor}}</td>
          <td>{{$produto->descricao}}</td>
          <td>

            <form method="post" action="{{ route('produtos.destroy', $produto->id) }}">
                <a href="{{route('produtos.edit',$produto->id)}}" class="btn btn-info">Editar</a>
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit" role="button">Deletar</button>
              </form>

          </td>
        </tr>
    </tbody>
  </table>






@endsection
