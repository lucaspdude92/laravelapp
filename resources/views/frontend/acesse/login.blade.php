@extends('layouts.frontend.applogin')
@section('content')


<div class="container">
    <div class="col-md-6 offset-3 ">

        <div class="login-page-content d-flex">

            <div class="spacer"></div>
            <a href="/" class="align-self-center"><img src="{{asset('images/logo.jpg')}}" class="img-fluid "></a>

            <div class="spacer"></div>

            <h2 class="align-self-center">ACESSO AO SISTEMA</h2>
            <H5 class="align-self-center">Insira seus dados abaixo para entrar.</H5>

            <div class="spacer"></div>
            <form action="">
                <div class="form-group">
                    <label for="cpf" >CPF:</label>
                    <input type="text" name="cpf" id="cpf" placeholder="Digite seu CPF" class="form-control">
                </div>

                <div class="form-group">
                    <label for="cpf">SENHA:</label>
                    <input type="password" name="senha" id="senha" placeholder="Digite sua senha" class="form-control">
                </div>

                <div class="content-center">
                    <div class="form-group">
                        <div class="center-content d-flex">
                            <button class="btn btn-blue btn-jumbo">Entrar</button>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="content-center d-flex">
                            <a href="">Esqueci minha senha!</a>
                        </div>
                    </div>

                </div>

            </form>
        </div>
    </div>
</div>



@endsection
