@extends('layouts.frontend.appfullscreen')
@section('content')



<div class="page-adquira-content d-flex">


    <div class="bs-stepper">
      <div class="col-md-12">
        <div class="bs-stepper-header" role="tablist">
          <div class="step" data-target="#inicio-part">
            <button type="button" class="step-trigger" role="tab" aria-controls="inicio-part" id="inicio-part-trigger">
              <span class="bs-stepper-circle">1</span>
              <span class="bs-stepper-label">INÍCIO</span>
            </button>
          </div>

          <div class="line"></div>
          <div class="step" data-target="#dados-part">
            <button type="button" class="step-trigger" role="tab" aria-controls="dados-part" id="dados-part-trigger">
              <span class="bs-stepper-circle">2</span>
              <span class="bs-stepper-label">PREENCHA SEUS DADOS</span>
            </button>
          </div>

          <div class="line"></div>
          <div class="step" data-target="#pagamento-part">
            <button type="button" class="step-trigger" role="tab" aria-controls="pagamento-part" id="pagamento-part-trigger">
              <span class="bs-stepper-circle">3</span>
              <span class="bs-stepper-label">FORMA DE PAGAMENTO</span>
            </button>
          </div>

          <div class="line"></div>
          <div class="step" data-target="#final-part">
            <button type="button" class="step-trigger" role="tab" aria-controls="final-part" id="final-part-trigger">
              <span class="bs-stepper-circle">4</span>
              <span class="bs-stepper-label">FIM!</span>
            </button>
          </div>
        </div>
      </div>
      <div class="spacer"></div>
      <div class="col-md-10 offset-1">
        <div class="bs-stepper-content">

          <div id="inicio-part" class="content stepper-content" role="tabpanel" aria-labelledby="inicio-part-trigger">


            <h2 class="center">Você está a um passo de ter seus dados médicos armazenados em um só lugar!</h2>

            <div class="spacer"></div>

            <p>O Meus Registros Médicos é um sistema que oferece a opção de cadastros de várias informações médicas, acessas via cartão físico através da leitura de um QR Code.</p>

            <div class="row">
              <div class="col">
                  <ul class="list-align-left">
                      <li>Tipo sanguíneo</li>
                      <li>Peso</li>
                      <li>Contatos de emergência</li>
                      <li>Plano de saúde</li>
                      <li>Hospitais de preferência</li>
                      <li>Histórico médico</li>
                      <li>Prescrição médica</li>
                      <li>Medicamentos</li>
                  </ul>
              </div>
              <div class="col">
                  <ul class="list-align-left">
                      <li>Prótese / Órtese</li>
                      <li>Alergias</li>
                      <li>Resultados de exames</li>
                      <li>Radiografia digital</li>
                      <li>Cartão de vacina</li>
                      <li>Profissionais médicos</li>
                      <li>Doador de órgãos</li>
                  </ul>
              </div>
          </div>

          <div class="spacer"></div>


          <div class="form-group">
            <label for="checkbox">
                <input type="checkbox" name=""checkbox id="checkbox"> Confirmo que as informações cadastradas <span class="bold">são de minha responsabilidade</span>.
            </label>
          </div>

          <div class="content-center">
            <button class="btn btn-blue-autline btn-jumbo">Voltar</button>
            <button class="btn btn-blue btn-jumbo">ESTOU CIENTE E QUERO ME CADASTRAR</button>
          </div>


          <div class="spacer"></div>
          <div class="small">O Meus Registros Médicos não se responsabiliza por dados errados inseridos no sistema</div>





          </div>

          <div id="dados-part" class="content stepper-content" role="tabpanel" aria-labelledby="dados-part-trigger">
            <h2 class="center">Vamos começar</h2>
            <div class="spacer"></div>
            <p><span class="bold">Insira seu CPF</span> e crie uma senha para o seu acesso ao sistema:</p>
            <div class="spacer"></div>

            <form action="">
              <div class="row">
                <div class="col">
                  <div class="form-group">
                    <label for="cpf">Digite seu CPF (ele será o seu login)</label>
                    <input type="text" name="cpf" id="cpf" class="form-control" placeholder="Insira seu CPF">
                  </div>
                </div>
                <div class="col">
                  <div class="form-group">
                    <label for="senha">Crie sua senha</label>
                    <input type="senha" name="senha" id="senha" class="form-control" placeholder="Insira seu senha">
                  </div>
                </div>
              </div>

              <div class="spacer"></div>
            </form>

            <div class="spacer"></div>

            <h2 class="center">Preencha seus dados</h2>
            <div class="spacer"></div>

            <div class="row">
              <div class="col-md-8">
                <div class="form-group">
                  <label class="label-left" for="nome">Nome Completo: </label>
                  <input type="text" name="nome" id="nome" class="form-control" placeholder="Nome completo">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label class="label-left" for="nascimento">Data de Nascimento: </label>
                  <input type="text" name="nascimento" id="nascimento" class="form-control" >
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="label-left" for="naturalidade">Naturalidade: </label>
                  <input type="text" name="naturalidade" id="naturalidade" class="form-control" >
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="label-left" for="nacionalidade">Nacionalidade: </label>
                  <input type="text" name="nacionalidade" id="nacionalidade" class="form-control" >
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label class="label-left" for="cep">CEP: </label>
                  <input type="text" name="cep" id="cep" class="form-control" >
                </div>
              </div>
              <div class="col-md-8">
                <div class="form-group">
                  <label class="label-left" for="endereco">Endereço: </label>
                  <input type="text" name="endereco" id="endereco" class="form-control" >
                </div>
              </div>
            </div>


            <div class="row">
              <div class="col-md-2">
                <div class="form-group">
                  <label class="label-left" for="numero">Número: </label>
                  <input type="text" name="numero" id="numero" class="form-control" >
                </div>
              </div>
              <div class="col-md-5">
                <div class="form-group">
                  <label class="label-left" for="complemento">Complemento: </label>
                  <input type="text" name="complemento" id="complemento" class="form-control" >
                </div>
              </div>
              <div class="col-md-5">
                <div class="form-group">
                  <label class="label-left" for="bairro">Bairro: </label>
                  <input type="text" name="bairro" id="bairro" class="form-control" >
                </div>
              </div>
            </div>


            <div class="row">
              <div class="col-md-8">
                <div class="form-group">
                  <label class="label-left" for="cidade">Cidade: </label>
                  <input type="text" name="cidade" id="cidade" class="form-control" >
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label class="label-left" for="endereco">Estado: </label>
                  <select class="form-control" >
                    <option >Espirito Santo</option>
                    <option>Minas Gerais</option>
                    <option>São Paulo</option>
                    <option>Rio de Janeiro</option>
                  </select>

                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="label-left" for="email">Email: </label>
                  <input type="text" name="email" id="email" class="form-control" >
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="label-left" for="telefone">Telefone: </label>
                  <input type="text" name="telefone" id="telefone" class="form-control" >
                </div>
              </div>
            </div>
            <div class="spacer"></div>
            <div class="content-center">
              <button class="btn btn-blue-autline btn-jumbo">Voltar</button>
              <button class="btn btn-blue btn-jumbo">Avançar</button>
            </div>



          </div>

          <div id="pagamento-part" class="content stepper-content" role="tabpanel" aria-labelledby="pagamento-part-trigger">
            <h2 class="center">Escolha sua forma de pagamento</h2>
            <div class="spacer"></div>

            <div class="formas-pagamento-wrapper">

              <div class="forma-pagamento-container">
                <div class="forma-pagamento-inner">
                  <div class="forma-pagamento-texto">Boleto Bancarário</div>
                  <div class="forma-pagamento-icone">
                    <i class="fas fa-money-check fa-2x"></i>

                  </div>
                </div>
              </div>

              <div class="forma-pagamento-container">
                <div class="forma-pagamento-inner">
                  <div class="forma-pagamento-texto">Cartão de Crédito</div>
                  <div class="forma-pagamento-icone">
                    <i class="fas fa-credit-card fa-2x"></i>
                  </div>
                </div>
              </div>

            </div>

            <div class="spacer"></div>
            <div class="content-center">
              <button class="btn btn-blue-autline btn-jumbo">Voltar</button>
              <button class="btn btn-blue btn-jumbo">Avançar</button>
            </div>

          </div>

          <div id="final-part" class="content stepper-content" role="tabpanel" aria-labelledby="final-part-trigger">
            <h2 class="center">Parabéns! Seu pedido foi realizado com sucesso!</h2>
            <div class="spacer"></div>
            <div class="content-center">
              <p>Você receberá por e-mail a confirmação de pagamento e acesso ao sistema.</p>
              <p>Seu cartão será entregue no endereço cadastrado</p>
            </div>

            <div class="spacer"></div>
            <div class="content-center">
              <button class="btn btn-blue-autline btn-jumbo">Voltar à Página Inicial</button>
            </div>
          </div>
        </div>
      </div>
    </div>

</div>


@endsection
