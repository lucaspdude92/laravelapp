
<section class="default-section accent-color-white adquirir">
    <div class="section-container">
        <div class="container">
            <div class="section-inner">
                <div class="col-md-12">
                    <h2 class="center">E quanto custa para adquirir o cartão?</h2>
                    <span>Apenas:</span>
                    <h2>R$ 119,00 / Ano</h2>
                    <p>É menos que <span class="bold">R$ 0,35 por dia!</span></p>

                    <a href="{{route('frontend.adquira')}}" class="btn btn-jumbo btn-white-outline">ADQUIRA AGORA</a>
                </div>


            </div>
        </div>
    </div>
</section>
