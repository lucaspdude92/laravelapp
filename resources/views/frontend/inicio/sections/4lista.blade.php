

<section class="default-section accent-color-blue">
    <div class="section-container">
        <div class="container">
            <div class="section-inner">
                <div class="col-md-12">
                    <h2 class="center">Com Meus <span class="red-accent">Registros Médicos </span>você só tem a ganhar</h2>
                </div>
                <div class="row">
                    <div class="col-md-5">
                        <img src="{{asset('images/frontend/ganhar-section.png')}}" class="img-fluid"  alt="">
                    </div>
                    <div class="col-md-7">

                        <p>Através do seu cartão, é muito simples acessar os seus dados médicos. Em uma situação de risco, ter essas informações em mãos, sempre atualizadas, pode ser decisivo para um tratamento emergêncial.</p>

                        <p>Através do sistema você tem a disposição as seguintes opções de cadastro:</p>

                        <div class="row">
                            <div class="col">
                                <ul class="list-simple">
                                    <li>Tipo sanguíneo</li>
                                    <li>Peso</li>
                                    <li>Contatos de emergência</li>
                                    <li>Plano de saúde</li>
                                    <li>Hospitais de preferência</li>
                                    <li>Histórico médico</li>
                                    <li>Prescrição médica</li>
                                    <li>Medicamentos</li>
                                </ul>
                            </div>
                            <div class="col">
                                <ul class="list-simple">
                                    <li>Prótese / Órtese</li>
                                    <li>Alergias</li>
                                    <li>Resultados de exames</li>
                                    <li>Radiografia digital</li>
                                    <li>Cartão de vacina</li>
                                    <li>Profissionais médicos</li>
                                    <li>Doador de órgãos</li>
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
