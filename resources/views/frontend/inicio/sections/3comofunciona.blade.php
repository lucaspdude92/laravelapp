
<section class="default-section accent-color-blue-reverse como-funciona">
    <div class="section-container">
        <div class="container">
            <div class="section-inner">
                <div class="row">
                    <div class="col-md-12">
                        <h2>Como funciona</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <span class="como-funciona-icons">
                            <img src="{{asset('images/frontend/icons/1.png')}}" class="img-fluid"  >
                        </span>
                        <h6 class="steps">Faça seu Cadastro</h6>
                    </div>

                    <div class="col-md-3">
                        <span class="como-funciona-icons">
                            <img src="{{asset('images/frontend/icons/2.png')}}" class="img-fluid"  >
                        </span>
                        <h6 class="steps">Preencha seus Dados</h6>
                    </div>

                    <div class="col-md-3">
                        <span class="como-funciona-icons">
                            <img src="{{asset('images/frontend/icons/3.png')}}" class="img-fluid"  >
                        </span>
                        <h6 class="steps">Escolha sua forma de pagamento</h6>
                    </div>

                    <div class="col-md-3">
                        <span class="como-funciona-icons">
                            <img src="{{asset('images/frontend/icons/4.png')}}" class="img-fluid"  >
                        </span>
                        <h6 class="steps">Receba seu cartão e pronto!</h6>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h5 class="como-funciona-text">Após concluído o cadastro, acesse seu perfil pela plataforma e complete com seus dados médicos.</h5>
                        <h5 class="como-funciona-text">As informações podem ser atualizadas sempre quue você quiser!</h5>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
