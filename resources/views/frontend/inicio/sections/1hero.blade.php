<section class="hero-section-container default-section">
    <div class="hero-section">
        <div class="container">
            <div class="col-md-12">

                <div class="hero-section-inner ">
                    <h2 class="hero-section-h2">Suas informações médicas sempre com você,</h2>
                    <h2 class="hero-section-h2">à um QR code de distância</h2>

                    <h3 class="hero-section-h3">Conheça o Meus Registros e edquira já o seu cartão!</h3>

                    <a href="{{ route('frontend.adquira') }}" class="btn btn-red">ADQUIRA</a>
                </div>


            </div>
        </div>
    </div>
</section>
