<section class="default-section accent-color-blue">
    <div class="section-container">
        <div class="container">
            <div class="section-inner">
                <div class="row">
                    <div class="col-md-7">
                        <h2>Acesse sua saúde onde você estiver</h2>
                        <p>Já pensou em ter todos os seus dados médicos em um único lugar, de fácil acesso e sempre atualizado? Essa é a proposta do Meus Registros Médicos!</p>

                        <p>Através de um dasdastro online e pagamento anual, você receberá em sua residência um cartão físico com um QR Code, que ao ser lido via celular ou tablet (através da câmera ou app leitor de QR Code), serão exibidos todos os seus dados médicos cadastrados em nosso sistema. Eles podem ser alterados sempre que nescessário, mantendo suas informações em dia.</p>

                        <p>Se interessou? <a href="{{route('frontend.adquira')}}" class="section-link-red">Clique aqui e faça o seu cadastro!</a></p>
                    </div>
                    <div class="col-md-5">
                        <img src="{{asset('images/frontend/cartao.png')}}" class="img-fluid img-cartao"  alt="">
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
