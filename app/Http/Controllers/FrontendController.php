<?php

namespace App\Http\Controllers;

use App\Pergunta;
use Illuminate\Http\Request;


class FrontendController extends Controller
{
    public function index(){

        // $duvidas = Pergunta::all();

        return view('frontend.inicio.index');
    }

    public function acesse(){
        return view('frontend.acesse.login');
    }

    public function adquira(){

        return view('frontend.adquira.index');
    }
}
