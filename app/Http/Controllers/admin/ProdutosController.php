<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Produto;
use Illuminate\Http\Request;

class ProdutosController extends Controller
{
    public function index(){
        $produtos = Produto::all();

        return view('backend.produtos.index')->with('produtos', $produtos);
    }

    public function show($id){
        $produto = Produto::find($id);
        return view('backend.produtos.show')->with('produto', $produto);
    }
    public function store(Request $request){

        $produto = new Produto();
        $produto->titulo = $request->titulo;
        $produto->descricao = $request->descricao;
        $produto->valor = $request->valor;
        $produto->ativo = true;
        $produto->tipo = $request->tipo;

        $produto->save();

        return redirect()->back();
    }

    public function edit($id){

        $produto = Produto::find($id);

        return view('backend.produtos.edit')->with('produto', $produto);

    }


    public function update($id, Request $request){

        $produto = Produto::find($id);
        $produto->titulo = $request->titulo;
        $produto->valor = $request->valor;
        $produto->descricao = $request->descricao;
        $produto->tipo = $request->tipo;

        $produto->update();

        return redirect()->route('produtos.index');


    }



    public function destroy($id){
        $produto = Produto::find($id);

        $produto->delete();

        return redirect()->route('produtos.index');
    }
}
