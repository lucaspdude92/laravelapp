<?php

namespace App\Http\Controllers\admin;

use App\Assinatura;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AssinaturasController extends Controller
{
    public function index(){
        $assinaturas = Assinatura::all();

        return view('backend.assinaturas.index')->with('assinaturas', $assinaturas);
    }
    public function show($id){

        $assinatura = Assinatura::find($id);

        return view('backend.assinaturas.show')->with('assinatura', $assinatura);
    }
}
