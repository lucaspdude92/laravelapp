<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Pergunta;
use Illuminate\Http\Request;

class PerguntasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perguntas = Pergunta::all();

        return view('backend.perguntas.index')->with('perguntas', $perguntas);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pergunta = new Pergunta();
        $pergunta->titulo = $request->titulo;
        $pergunta->conteudo = $request->conteudo;
        $pergunta->save();

        return redirect()->route('perguntas.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pergunta = Pergunta::find($id);

        return view('backend.perguntas.show')->with('pergunta', $pergunta);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pergunta = Pergunta::find($id);

        return view('backend.perguntas.edit')->with('pergunta', $pergunta);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pergunta = Pergunta::find($id);

        $pergunta->titulo = $request->titulo;
        $pergunta->conteudo = $request->conteudo;
        $pergunta->update();

        return redirect()->route('perguntas.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pergunta = Pergunta::find($id);

        $pergunta->delete();

        return redirect()->route('perguntas.index');
    }
}
