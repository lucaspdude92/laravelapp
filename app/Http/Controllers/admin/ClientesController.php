<?php

namespace App\Http\Controllers\admin;

use App\Cliente;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;

class ClientesController extends Controller
{
    public function index(Cliente $clientes){

        $clientes = Cliente::all();

        return view('backend.clientes.index')->with('clientes', $clientes);
    }

    public function show($id){
        $cliente = Cliente::find($id);

        return view('backend.clientes.show')->with('cliente', $cliente);
    }

    public function edit($id){
        $cliente = Cliente::find($id);
        return view('backend.clientes.edit')->with('cliente', $cliente);
    }
    public function update($id, Request $request){

        $cliente = Cliente::find($id);

        $cliente->nomecompleto = $request->nomecompleto;
        $cliente->datanascimento = $request->datanascimento;
        $cliente->naturalidade = $request->naturalidade;
        $cliente->nacionalidade = $request->nacionalidade;
        $cliente->cep = $request->cep;
        $cliente->endereco = $request->endereco;
        $cliente->numero = $request->numero;
        $cliente->complemento = $request->complemento;
        $cliente->bairro = $request->bairro;
        $cliente->cidade = $request->cidade;
        $cliente->estado = $request->estado;
        $cliente->email = $request->email;
        $cliente->telefone = $request->telefone;

        $cliente->update();

        return redirect()->back()->with('message', 'Dados atualizados com sucesso!');
    }


    public function destroy($id){
        $cliente = Cliente::find($id);
        $cliente->delete();

        return redirect()->back();
    }


}
