<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $fillable = [
        'datanascimento',
        'naturalidade',
        'nacionalidade',
        'cep',
        'endereco',
        'numero',
        'complemento',
        'bairro',
        'cidade',
        'estado',
        'email',
        'telefone',
    ];


    public function user(){
        return $this->belongsTo('App\User');
    }

    public function assinaturas(){
        return $this->hasMany('App\Assinatura', 'clientes_id');
    }


}
