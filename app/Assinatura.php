<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assinatura extends Model
{
    public function clientes(){

        return $this->belongsTo('App\Cliente');
    }

    public function produtos(){
        return $this->belongsTo('App\Produto');
    }
}
