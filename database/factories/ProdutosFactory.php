<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Produto;
use Faker\Generator as Faker;

$factory->define(Produto::class, function (Faker $faker) {
    return [
        'titulo' => $faker->sentence(3),
        'descricao' => $faker->paragraph(),
        'valor' => $faker->numberBetween(110.25, 150.36),
        'ativo' => true,
        'tipo' => 'Único'
    ];
});
