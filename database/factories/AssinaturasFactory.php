<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Assinatura;
use App\Model;
use Faker\Generator as Faker;

$factory->define(Assinatura::class, function (Faker $faker) {
    return [
        'produtos_id' => $faker->numberBetween(1,5),
    ];
});
