<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Pergunta;
use Faker\Generator as Faker;

$factory->define(Pergunta::class, function (Faker $faker) {
    return [
        'titulo' => $faker->sentence(),
        'conteudo' => $faker->paragraph(),
    ];
});
