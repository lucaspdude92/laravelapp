<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Cliente;
use App\Model;
use Faker\Generator as Faker;

$factory->define(Cliente::class, function (Faker $faker) {
    return [
        'nomecompleto' => $faker->name(),
        'datanascimento' => $faker->date,
        'naturalidade' => $faker->word(),
        'nacionalidade' => $faker->word(),
        'cep' => $faker->numberBetween(1,150),
        'endereco' => $faker->address(),
        'numero' => $faker->numberBetween(1,150),
        'complemento' => $faker->sentence(),
        'bairro' => $faker->word(),
        'cidade' => $faker->word(),
        'estado' => $faker->word(),
        'email' => $faker->email,
        'telefone' => $faker->word,
    ];
});


