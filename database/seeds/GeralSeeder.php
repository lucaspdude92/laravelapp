<?php

use App\Assinatura;
use App\Cliente;
use App\Pergunta;
use App\Produto;
use App\User;
use Illuminate\Database\Seeder;
use Carbon\Carbon;


class GeralSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        $produtos = factory(Produto::class, 5)->create();
        $perguntas = factory(Pergunta::class, 5)->create();



        $usuarios = factory(User::class, 30)->create()->each(function($usuario){
            $clientes = factory(Cliente::class)->create([
                'user_id' => $usuario->id
            ]);
            $current_date_time = Carbon::now()->toDateTimeString();
            $one_year = date("Y-m-d", strtotime(date("Y-m-d", strtotime($current_date_time)) . " + 1 year"));


            $assinaturas = factory(Assinatura::class)->create([
                    'clientes_id' => $usuario->id,
                    'inicio' => $current_date_time,
                    'fim' => $one_year
            ]);



        });
    }
}
