<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFichasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fichas', function (Blueprint $table) {
            $table->id();
            $table->string('tiposanguineo');
            $table->string('peso');
            $table->string('planosaude');
            $table->text('historicomedico');
            $table->text('prescricoes');
            $table->string('proteses');
            $table->string('alergiasmedicamentos');
            $table->string('alergiasalimentos');
            $table->string('doadororgaos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fichas');
    }
}
